﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AidKitHandler : MonoBehaviour
{
    public Rigidbody player;
    public GameObject healthBarPrefab;

    public IMessage message;
    public int maxHP;

    float hp;
    float bulletSpeed;

    FollowingHealthBar healthBar;

    float spawnTimer = 7f;
    float timer = 0f;


    // Use this for initialization
    void Start()
    {
        hp = maxHP;
        InitHealthBar();

    }

    void InitHealthBar()
    {
        GameObject bar = Instantiate(healthBarPrefab);
        healthBar = bar.GetComponent<FollowingHealthBar>();
        healthBar.Init(transform, player.transform, maxHP);
    }

    // Update is called once per frame
    void Update()
    {
        AimAtPlayer();
    }
    void AimAtPlayer()
    {
        Vector3 v = PredictPlayerPosition();
        Quaternion rotation = Quaternion.LookRotation(v - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 60);
        //transform.LookAt(v);
        v.y -= 0.5f;
    }


    Vector3 PredictPlayerPosition()
    {
        return player.position + player.velocity * bulletSpeed * Time.deltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        Ammo ammo;
        if (ammo = collision.gameObject.GetComponent<Ammo>())
        {
            TakeDamage(ammo.damage);
        }
    }



    void TakeDamage(float damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            hp = 0;
            Destroy(gameObject);
            RecivingDamage.hp = 20;
        }
        else
            healthBar.UpdateHP(hp);

    }
}
