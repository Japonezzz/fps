﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootStarter : MonoBehaviour
{
    public static bool entered = false;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<EnemyHandler>())
            entered = true;
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<EnemyHandler>())
            entered = false;
    }
}
